local LookingForGroup = LibStub("AceAddon-3.0"):GetAddon("LookingForGroup")
local Hook = LookingForGroup:NewModule("Hook","AceHook-3.0")

function Hook:OnInitialize()
	local profile = LookingForGroup.db.profile
--	self:SecureHook("QuestObjectiveSetupBlockButton_FindGroup")
--	self:SecureHook("QuestObjectiveReleaseBlockButton_FindGroup")
	self:SecureHook("QuestWatch_Update")
	local disable_pve_frame
	local select = select
	local GetAddOnMetadata = GetAddOnMetadata
	local GetAddOnInfo = GetAddOnInfo
	for i = 1, GetNumAddOns() do
		if GetAddOnMetadata(i, "X-LFG-DISABLE-PVEFRAME") then
			local name, title, notes, loadable, reason, security, newVersion = GetAddOnInfo(i)
			if loadable or reason == "DEMAND_LOADED" then
				disable_pve_frame = true
			end
		end
	end
	if profile.disable_gmotd then
		tremove(ChatTypeGroup.GUILD,2)
	end
	LookingForGroup.disable_pve_frame = disable_pve_frame
	if disable_pve_frame then
		self:RawHook("PVEFrame_ShowFrame",function()
			LookingForGroup:SendMessage("LFG_ICON_LEFT_CLICK")
		end,true)			-- lol. RawHook is correct while SecureHook is wrong here!
	end
	self.OnInitialize=nil
	self.quest_objective_pool = CreateFramePool("BUTTON", UIParent, "QuestObjectiveFindGroupButtonTemplate");
end

function Hook.quest_group_onclick(button,key,unknown)
	if not IsInInstance() and not LookingForGroup:loadevent("LookingForGroup_Q","LFG_SECURE_QUEST_ACCEPTED",button.questID,key=="RightButton") then
		LookingForGroup:Print("LookingForGroup_Q failed to load")
	end
end

function Hook:QuestObjectiveSetupBlockButton_FindGroup(block, questID)
	local button = self.quest_objective_pool:Acquire();
	button.questID = questID
	button:RegisterForClicks("AnyUp")
	button:SetScript("OnClick",self.quest_group_onclick)
	button:ClearAllPoints()
	LookingForGroup:SendMessage("LFG_QUEST_BUTTON",button,questID,block)
	if button:GetNumPoints() == 0 then
		if Questie then
			button:SetPoint("TOPRIGHT", block, "TOPLEFT",-40,0)
		else
			button:SetPoint("TOPRIGHT", block, "TOPLEFT",-5,0)
		end
	end
	button:Show()
end
local GetNumQuestWatches = GetNumQuestWatches
function Hook:QuestWatch_Update(...)
	self.quest_objective_pool:ReleaseAll()
	local watchTextIndex = 1
	local GetNumQuestLeaderBoards = GetNumQuestLeaderBoards
	for i=1, GetNumQuestWatches() do
		local questIndex = GetQuestIndexForWatch(i)
		if ( questIndex ) then
			local title, level, suggestedGroup, isHeader, isCollapsed, isComplete, frequency, questID, startEvent, displayQuestID, isOnMap, hasLocalPOI, isTask, isBounty, isStory, isHidden, isScaling =  GetQuestLogTitle(questIndex)
			local numObjectives = GetNumQuestLeaderBoards(questIndex)
			if 0 < numObjectives then
				Hook:QuestObjectiveSetupBlockButton_FindGroup(_G["QuestWatchLine"..watchTextIndex],questID)
				watchTextIndex = 1+watchTextIndex + numObjectives
			end
		end
	end
end