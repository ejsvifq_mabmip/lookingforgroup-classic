local LookingForGroup = LibStub("AceAddon-3.0"):GetAddon("LookingForGroup")
local LookingForGroup_Q = LibStub("AceAddon-3.0"):NewAddon("LookingForGroup_Q","AceEvent-3.0")

function LookingForGroup_Q:OnInitialize()
	local db = LookingForGroup.db
	local defaults = LookingForGroup.db.defaults
	defaults.profile.q = {}
	db:RegisterDefaults(defaults)
end

function LookingForGroup_Q:OnEnable()
	LookingForGroup_Q:RegisterEvent("QUEST_ACCEPTED")
	LookingForGroup_Q:RegisterMessage("LFG_SECURE_QUEST_ACCEPTED")
end

local function cofunc(quest_id,secure,gp)
	local GetQuestLogTitle = GetQuestLogTitle
	local questName
	for i=1,GetNumQuestLogEntries() do
		local title, level, suggestedGroup, isHeader, isCollapsed, isComplete, frequency, questID, startEvent, displayQuestID, isOnMap, hasLocalPOI, isTask, isBounty, isStory, isHidden, isScaling = GetQuestLogTitle(i)
		if questID == quest_id then
			if secure <= 0 and frequency == LE_QUEST_FREQUENCY_WEEKLY then
				return
			end
			questName = title
			break
		end
	end
	if questName == nil then return end
	local confirm_keyword = tostring(quest_id)
	local function create()
	end
	LookingForGroup_Q:RegisterEvent("QUEST_REMOVED",function(info,id)
		if quest_id == id and LookingForGroup.popup and LookingForGroup.popup:IsShown() then
			LookingForGroup.popup:Hide()
		end
	end)
	local raid = select(4,GetQuestTagInfo(quest_id)) == 3
	if not gp and IsInGroup() then
		if 0 < secure and UnitIsGroupLeader("player", LE_PARTY_CATEGORY_HOME) then
			gp = true
		else
			return
		end
	end
	local current = coroutine.running()
	if LookingForGroup.accepted(questName,nil,create,secure,false,confirm_keyword,"<LFG>Q",gp) then
		return
	end
	LookingForGroup_Q:RegisterEvent("QUEST_ACCEPTED",function(event,index,id)
		if IsInGroup() then
			if quest_id == id then
				LookingForGroup.resume(current,3)
			end
		else
			LookingForGroup.resume(current)
			LookingForGroup_Q:RegisterEvent("QUEST_ACCEPTED")
		end
	end)
	LookingForGroup_Q:RegisterEvent("QUEST_TURNED_IN",function(info,id)
		if quest_id == id then
			LookingForGroup.resume(current,0,gp)
		end
	end)
	LookingForGroup_Q:RegisterEvent("QUEST_REMOVED",function(info,id)
		if quest_id == id then
			LookingForGroup.resume(current,1,gp)
		end
	end)
	LookingForGroup.autoloop(questName,create,raid,confirm_keyword,"<LFG>Q",function()
		local distance = C_TaskQuest.GetDistanceSqToQuest(quest_id)
		return not distance or distance < 40000
	end)
	LookingForGroup_Q:UnregisterEvent("QUEST_TURNED_IN")
	LookingForGroup_Q:UnregisterEvent("QUEST_REMOVED")
	LookingForGroup_Q:RegisterEvent("QUEST_ACCEPTED")
end

local function is_group_q(id,ignore)
	if id == nil or IsRestrictedAccount() then
		return
	end
	local profile = LookingForGroup.db.profile
	if ignore then
		return true
	end
	if GetQuestLogRewardMoney(id) == 0 then
		return
	end
	if (46794 <= id and id <= 46802) -- legion paragon quests
		or (50598<=id and id <= 50606) -- bfa bounty quests
		or (51021<=id and id <= 51051) or (52375<=id and id <= 52388) -- bfa supplies needed
		or (54134<=id and id <= 54138) --bfa assaults
		or (54626<=id and id <= 54632) -- bfa paragon quests
		or (56006<=id and id <= 56022 and id~= 56012 and id~=56017) -- 8.2 Runelocked Chest
		or (56023<=id and id <=56025) -- 8.2 Laylocked Chest
		then return
	end
	if profile.q[id] then
		return
	end
	local tagID, tagName, wq_type, rarity, isElite, tradeskillLineIndex, displayTimeLeft = GetQuestTagInfo(id)
	if tagID == 62 or tagID == 81 or tagID == 83 or tagID == 117 or tagID == 124 or tagID == 125 then
		return
	end
	if profile.auto_wq_only and wq_type == nil then
		return
	end
	if wq_type == LE_QUEST_TAG_TYPE_PET_BATTLE or wq_type == LE_QUEST_TAG_TYPE_PROFESSION or wq_type == LE_QUEST_TAG_TYPE_DUNGEON or wq_type == LE_QUEST_TAG_TYPE_RAID then
		return
	end
	if math.floor(id/100) == 413 then
		return
	end
	return true
end

function LookingForGroup_Q:QUEST_ACCEPTED(_,index,quest_id)
	local load_time = LookingForGroup.load_time
	if load_time == nil or GetTime() < load_time + 5 then
		return
	end
	if is_group_q(quest_id) then
		coroutine.wrap(cofunc)(quest_id,0)
	end
end

function LookingForGroup_Q:LFG_SECURE_QUEST_ACCEPTED(_,quest_id,group)
	if is_group_q(quest_id,true) then
		coroutine.wrap(cofunc)(quest_id,1,group)
	end
end
