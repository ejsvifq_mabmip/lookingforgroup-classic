## Interface: 11302
## Author: cqwrteur
## Title: LookingForGroup Options
## Version: @project-version@
## SavedVariables: LookingForGroup_OptionsDB
## LoadOnDemand: 1
## X-LFG-MESSAGE: LFG_ChatCommand,LFG_ICON_LEFT_CLICK,LFG_UPDATE_EDITING
## Dependencies: LookingForGroup

#@no-lib-strip@
Libs\AceGUI-3.0\AceGUI-3.0.xml
Libs\AceConfig-3.0\AceConfig-3.0.xml
Libs\AceDBOptions-3.0\AceDBOptions-3.0.xml
Libs\AceLocale-3.0\AceLocale-3.0.xml
#@end-no-lib-strip@

Init.lua
module.xml
Core.lua