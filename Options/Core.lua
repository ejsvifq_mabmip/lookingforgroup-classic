local AceAddon = LibStub("AceAddon-3.0")
local LookingForGroup = AceAddon:GetAddon("LookingForGroup")
local LookingForGroup_Options = AceAddon:GetAddon("LookingForGroup_Options")

function LookingForGroup_Options:OnEnable()
	local options = LookingForGroup_Options.option_table
	LibStub("AceConfig-3.0"):RegisterOptionsTable("LookingForGroup", options)
	options.args.profile = LibStub("AceDBOptions-3.0"):GetOptionsTable(LookingForGroup_Options.db)
	self.db.RegisterCallback(self, "OnProfileChanged", "OnProfileChanged")
	self.db.RegisterCallback(self, "OnProfileCopied", "OnProfileChanged")
	self.db.RegisterCallback(self, "OnProfileReset", "OnProfileChanged")
	local GetAddOnMetadata = GetAddOnMetadata
	local GetAddOnInfo = GetAddOnInfo
	local region = GetCurrentRegion()
	for i = 1, GetNumAddOns() do
		local metadata = GetAddOnMetadata(i, "X-LFG-OPT")
		if metadata and (metadata == "0" or region == tonumber(metadata)) then
			LoadAddOn(i)
		end
	end
	self:OnProfileChanged()
	self.option_table.args.settings=
	{
		name = SETTINGS,
		type = "group",
		args =
		{
			enable =
			{
				name = ENABLE,
				type = "execute",
				func = function()
					LoadAddOn("LookingForGroup_Settings")
					collectgarbage("collect")
					LookingForGroup_Options:SendMessage("LFG_SETTINGS_ENABLED")
				end
			}
		}
	}
	self.OnEnable = nil
	self.OnInitialize = nil
	self:RegisterMessage("LFG_ICON_LEFT_CLICK")
end

function LookingForGroup_Options.IsSelected(groupname)
	local status_table = LibStub("AceConfigDialog-3.0"):GetStatusTable("LookingForGroup")
	if status_table.groups and status_table.groups.selected == groupname then
		return true
	end
end

function LookingForGroup_Options.NotifyChangeIfSelected(groupname)
	if LookingForGroup_Options.IsSelected(groupname) then
		LibStub("AceConfigRegistry-3.0"):NotifyChange("LookingForGroup")
		return true
	end
end

function LookingForGroup_Options.OnProfileChanged(update_db)
end

function LookingForGroup_Options:LFG_ChatCommand(message,input)
	if not input or input:trim() == "" then
		LibStub("AceConfigDialog-3.0"):Open("LookingForGroup")
	else
		LibStub("AceConfigCmd-3.0"):HandleCommand("LookingForGroup", "LookingForGroup",input)
	end
end

function LookingForGroup_Options:LFG_ICON_LEFT_CLICK(message,para,...)
	local AceConfigDialog = LibStub("AceConfigDialog-3.0")
	if AceConfigDialog.OpenFrames.LookingForGroup then
		AceConfigDialog:Close("LookingForGroup")
	else
		if para then
			AceConfigDialog:SelectGroup(para,...)
		end
		AceConfigDialog:Open("LookingForGroup")
	end
end

function LookingForGroup_Options:LFG_UPDATE_EDITING()
	self.update_editing()
	local AceConfigDialog = LibStub("AceConfigDialog-3.0")
	AceConfigDialog:SelectGroup("LookingForGroup","find","s")
	AceConfigDialog:Open("LookingForGroup")
end

function LookingForGroup_Options.AJ_PVE_LFG_ACTION()
	local AceConfigDialog = LibStub("AceConfigDialog-3.0")
	AceConfigDialog:SelectGroup("LookingForGroup","find")
	AceConfigDialog:Open("LookingForGroup")
end

LookingForGroup_Options.AJ_PVP_LFG_ACTION = LookingForGroup_Options.AJ_PVE_LFG_ACTION
