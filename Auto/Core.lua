local LookingForGroup = LibStub("AceAddon-3.0"):GetAddon("LookingForGroup")
local Auto = LookingForGroup:NewModule("Auto","AceEvent-3.0")

local ms = LibStub("AceAddon-3.0"):GetAddon("MeetingStone", true)
if ms then
	local cp = ms:GetModule("CreatePanel", true)
	if cp then
		cp:UnregisterEvent("LFG_LIST_ENTRY_CREATION_FAILED")
		cp.LFG_LIST_ENTRY_CREATION_FAILED = nil
	end
end

local function is_queueing_lfg()
	for i = 1,6 do
		if GetLFGMode(i) then
			return true
		end
	end
	for i=1,GetMaxBattlefieldID() do
		if GetBattlefieldStatus(i) ~= "none" then
			return true
		end
	end
end

function LookingForGroup.accepted(name,search,create,secure,raid,keyword,ty_pe,create_only,composition)
	local profile = LookingForGroup.db.profile
	if (secure <= 0 and profile.disable_auto) or is_queueing_lfg() or LookingForGroup.auto_is_running then
		return true
	end
end

function LookingForGroup.autoloop(name,create,raid,keyword,ty_pe,in_range,composition)
	Auto:SendMessage("LFG_AUTO_MAIN_LOOP",keyword)
	LookingForGroup.auto_is_running = name
	local current = coroutine.running()
	local profile = LookingForGroup.db.profile
	local function event_func(...)
		LookingForGroup.resume(current,...)
	end
	Auto:UnregisterEvent("GROUP_ROSTER_UPDATE")
	local ticker
	local function initialize_kicker()
		if ticker then
			ticker:Cancel()
		end
		if profile.auto_kick then
			ticker = C_Timer.NewTicker(1,function()
				event_func(13)
			end)
		end
	end
	initialize_kicker()
	local player_list
	local original_tp = ty_pe
	local invited_tb = {}
	local has_set_friends
	if not profile.auto_addons_wqt and in_range then
		Auto:RegisterEvent("CHAT_MSG_SYSTEM",event_func)
		UIParent:UnregisterEvent("GROUP_INVITE_CONFIRMATION")
		Auto:RegisterEvent("GROUP_INVITE_CONFIRMATION",event_func)
		if not profile.auto_show_nameplate then
			local function callback()
				has_set_friends = not GetCVarBool("nameplateShowFriends")
				SetCVar("nameplateShowFriends",true)
			end
			if InCombatLockdown() then
				Auto:RegisterEvent("PLAYER_REGEN_ENABLED",callback)
			else
				callback()				
			end
		end
		Auto:RegisterEvent("NAME_PLATE_UNIT_ADDED",event_func,18)
		Auto:RegisterEvent("UNIT_HEALTH_FREQUENT",event_func,18)
		Auto:RegisterEvent("UNIT_TARGET",event_func,17)
	end
	if keyword then
		Auto:RegisterEvent("CHAT_MSG_WHISPER",event_func)
		if ty_pe then
			ty_pe = ty_pe..keyword
		else
			ty_pe = keyword
		end
		player_list = {}
	end
	local must
	Auto:RegisterMessage("LFG_AUTO_MAIN_LOOP",event_func)
	Auto:RegisterMessage("LFG_ICON_MIDDLE_CLICK",event_func)
	local function full()
		local q = raid
		local n = GetNumGroupMembers()
		local tn = n
		local maxn = q and 40 or 5
		if q then
			maxn = 40
		else
			maxn = 5
		end
		if maxn <= tn then
			return true
		end
		local gtime = GetTime()
		local counter = tn
		local type = type
		for k,v in pairs(invited_tb) do
			if type(v)=="number" then
				if v + 120 < gtime then
					invited_tb[k] = true
				else
					counter = counter + 1
					if maxn <= counter then
						return true
					end
				end
			end
		end	
	end
	local show_popup = LookingForGroup.show_popup
	local not_hide_popup
	while true do
		local k,gpl,arg3,arg4,arg5,arg6 = coroutine.yield()
		if is_queueing_lfg()  or k == "GROUP_LEFT" then
			break
		elseif k == 0 or k == 1 then
			local nm = GetNumGroupMembers()
			local auto_leave_party = profile.auto_leave_party
			if nm == 0 or (k == 0 and (nm == 1 or (not gpl and auto_leave_party))) then
				LeaveParty()
				break
			elseif k == 0 and gpl then
			else
				local tb = {nop,ACCEPT,LeaveParty}
				if UnitIsGroupLeader("player") then
					tb[#tb+1]=UNLIST_MY_GROUP
					tb[#tb+1]=function()
						event_func("GROUP_LEFT")
					end
				end
				show_popup(PARTY_LEAVE,tb)
				if k == 0 then
					not_hide_popup = true
					break
				end
			end
		elseif k == 3 then
			if LookingForGroup.popup then
				LookingForGroup.popup:Hide()
			end
		elseif k == 11 then
			if not IsInInstance() then
				LeaveParty()
			end
		elseif k == 17 or k == 18 then
			if (not IsInGroup() or UnitIsGroupLeader("player") or UnitIsGroupAssistant("player")) and UnitExists(arg3) and
				not UnitInAnyGroup(arg3) and UnitIsPlayer(arg3) and UnitIsFriend(arg3,"player") and
				not UnitOnTaxi(arg3) and (UnitAffectingCombat(arg3) or (k==17 and GetUnitSpeed(arg3) == 0)) then
				local q = raid
				local n = GetNumGroupMembers()
				local tn = n
				local maxn = q and 40 or 5
				if tn < maxn  then
					local name,server = UnitName(arg3)
					if name and server then
						name = name.."-"..server
					end
					if name and not invited_tb[name] then
						local us = UnitInRaid("player") and "raid" or "party"
						for i = 1, (us=="party" and n-1 or n) do
							local uname,userver = UnitName(us..i)
							if uname and userver then
								uname = uname.."-"..userver
							end
							if uname then
								invited_tb[uname] = true
							end
						end
						if not full() then
							InviteUnit(name)
							invited_tb[name] = GetTime()
						end
					end
				end
			end
		elseif k == 19 or k == 20 then
			local UnitDistanceSquared = UnitDistanceSquared
			local UnitIsUnit = UnitIsUnit
			local UnitExists = UnitExists
			local UnitIsConnected = UnitIsConnected
			local q = raid
			local m = min((q and 40 or 5),n)
			local n = GetNumGroupMembers()
			local require_kick
			repeat
			local u = UnitInRaid("player") and "raid" or "party"
			for i=1,(u=="party" and m-1 or m) do
				local unit = u .. i
				local distance = UnitDistanceSquared(unit)
				local name,server = UnitName(unit)
				if name and UnitExists(unit) and not UnitIsUnit("player",unit) and (not UnitIsConnected(unit) or (not distance or 1500000 < distance)) then
					if k==20 then
						UninviteUnit(unit)
					else
						require_kick = name.."-"..server
						break
					end
				end
			end
			for i=m+1,n do
				local unit = u .. i
				if k==20 then
					UninviteUnit(unit)
				else
					require_kick = "Full!"
					break
				end
			end
			until true
			if (not q) and IsInRaid() and (k == 20 or not require_kick) then
				ConvertToParty()
			end
			if k==19 and require_kick then
				show_popup("Kick",{nop,require_kick,function() event_func(20) end})
				ticker:Cancel()
			end
			if k==20 then
				initialize_kicker()
			end
		elseif k == "CHAT_MSG_WHISPER" then
			if not full() and gpl == ty_pe and (not player_list[arg3] or player_list[arg3] + 30 < GetTime() ) then
				player_list[arg3] = GetTime()
				InviteUnit(arg3)
			end
		elseif k == "CHAT_MSG_SYSTEM" then
			local uname = string.match(gpl,string.gsub(ERR_DECLINE_GROUP_S,"%%s","(.*)"))
			if uname then
				uname = strsplit("-",uname)
				for k,v in pairs(invited_tb) do
					if uname == strsplit("-",k) then
						invited_tb[k] = true
					end
				end
			end
		elseif k == "GROUP_INVITE_CONFIRMATION" then
			local firstInvite = GetNextPendingInviteConfirmation()
			if firstInvite then
				local confirmationType, name, guid, rolesInvalid, willConvertToRaid = GetInviteConfirmationInfo(firstInvite)
				if not full() and confirmationType == LE_INVITE_CONFIRMATION_REQUEST and invited_tb[name] == true then
					RespondToInviteConfirmation(firstInvite, true)
				else
					RespondToInviteConfirmation(firstInvite, false)
				end
			end
		else
			break
		end
	end
	if not not_hide_popup and LookingForGroup.popup then
		LookingForGroup.popup:Hide()
	end
	LookingForGroup.auto_is_running = nil
	UIParent:RegisterEvent("GROUP_INVITE_CONFIRMATION")
	Auto:UnregisterAllEvents()
	Auto:UnregisterAllMessages()
	if ticker then ticker:Cancel() end
	if has_set_friends and GetCVarBool("nameplateShowFriends") then
		local function callback()
			SetCVar("nameplateShowFriends",false)
			Auto:UnregisterEvent("PLAYER_REGEN_ENABLED")
		end
		if InCombatLockdown() then
			Auto:RegisterEvent("PLAYER_REGEN_ENABLED",callback)
		else
			callback()
		end
	end
end
